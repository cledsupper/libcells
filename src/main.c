#include <stdio.h>
#include <string.h>

#define USING_CELL_LANG
#include <cells/cell.h>

DECLARE(my_cell)

struct my_cell_public {
    SUPER_T(cell)
};

struct my_cell_private {
    char code[10];
};

struct my_cell_code_public {
    SUPER_C(cell)
};

DEFINE(cell, my_cell)

static void init_code() {}

static void init(my_cell_t * self) {
    struct my_cell_private * priv = __(self);
    strcpy(priv->code, "hvftA467g");
}

my_cell_t * my_cell_new() {
    get_my_cell_code();
    return cell_new(&C);
}

const char * my_cell_get_code(const my_cell_t * self) {
    return __(self)->code;
}

int main() {
    auto my_cell_t * mc = my_cell_new();
    puts(my_cell_get_code(mc));
    unref(mc);
    mc = NULL;
    return 0;
}
