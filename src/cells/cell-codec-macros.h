/** @file cell-codec-macros.h
 * @brief Macros para facilitar a codificação das células.
 * @copyright Copyright 2022 Cledson Ferreira. All rights reserved.
 */

/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LESSO_CELL_H

#error Include cell.h instead!

#elif !defined(_LESSO_CODEC_MACROS)
#define _LESSO_CODEC_MACROS

/** Declarações de tipos e métodos básicos. */
#define DECLARE(MY_TYPE)						\
typedef struct MY_TYPE##_public	MY_TYPE##_t;			\
typedef struct MY_TYPE##_code_public	MY_TYPE##_c;			\
									\
void MY_TYPE##_init(any_t cell, any_t code) NONNULL(1, 2);		\
									\
bool is_##MY_TYPE(const_any_t cell) NONNULL(1);			\
bool is_##MY_TYPE##_code(const_any_t code) NONNULL(1);		\
MY_TYPE##_t * MY_TYPE##_check(const_any_t cell) NONNULL(1);		\
const MY_TYPE##_c * MY_TYPE##_code_check(const_any_t code) NONNULL(1);\
									\
MY_TYPE##_c get_##MY_TYPE##_code();					\
									\
const MY_TYPE##_c * MY_TYPE##_code(const_any_t cell) NONNULL(1);


/** Declara variáveis de uso privado do código celular e implementa funções. */
#define DEFINE(SUPER_TYPE, MY_TYPE)					\
static SUPER_TYPE##_c super;						\
static MY_TYPE##_c C;							\
static size_t PRIVATE_INDEX = 0;					\
static bool __init_completed = false;					\
									\
static void init_code();					\
static void init(MY_TYPE##_t * self);				\
									\
void MY_TYPE##_init(any_t cell, any_t code) {			\
    if (!__init_completed) get_##MY_TYPE##_code();			\
    SUPER_TYPE##_init(cell, code);					\
    MY_TYPE##_t * self = (MY_TYPE##_t *) cell;			\
    self->BASE = C.BASE;						\
    init(self);							\
}									\
									\
bool is_##MY_TYPE(const_any_t cell) {					\
    if (!is_cell(cell)) return false;					\
    cell_t * c_self = (cell_t *) cell;				\
    cell_c * root = (cell_c *)&C;					\
    bool ok = false;							\
    if (!__init_completed) return ok;					\
    if (c_self->C == root) ok = true;					\
    else if (c_self->C->CELL_SIZE >= root->CELL_SIZE) {		\
        MY_TYPE##_t * self = (MY_TYPE##_t *) cell;			\
        if (self->BASE == C.BASE) ok = true;			\
    }									\
    return ok;								\
}									\
									\
bool is_##MY_TYPE##_code(const_any_t code) {				\
    if (!is_cell_code(code)) return false;				\
    cell_c * root = (cell_c *)&C;					\
    bool ok = false;							\
    if (!__init_completed) return ok;					\
    if (code == root) ok = true;					\
    else if (((cell_c *)code)->CODE_SIZE >= root->CODE_SIZE) {	\
        MY_TYPE##_c * m_code = (MY_TYPE##_c *) code;			\
        if (m_code->BASE == C.BASE) ok = true;			\
    }									\
    return ok;								\
}									\
									\
MY_TYPE##_t * MY_TYPE##_check(const_any_t cell) {			\
    if (!is_##MY_TYPE(cell))						\
        libcells_abort(						\
          __MODULE__,							\
          __func__,							\
          __LINE__,							\
          "array/struct != '%s'",					\
          #MY_TYPE							\
        );								\
   return (MY_TYPE##_t *) cell;					\
}									\
									\
const MY_TYPE##_c * MY_TYPE##_code_check(const_any_t code) {	\
    if (!is_##MY_TYPE##_code(code))					\
        libcells_abort(						\
          __MODULE__,							\
          __func__,							\
          __LINE__,							\
          "array/struct != '%s'",					\
          #MY_TYPE							\
        );								\
    return (const MY_TYPE##_c *) code;				\
}									\
									\
MY_TYPE##_c get_##MY_TYPE##_code() {					\
    if (!__init_completed) {						\
        super = get_##SUPER_TYPE##_code();				\
        C.super = super;						\
        C.BASE = &C;							\
        cell_c * root = (cell_c *) &C;				\
        root->CELL_PATH = __MODULE__;					\
        root->CELL_TYPE = #MY_TYPE;					\
        root->CELL_SIZE = libcells_size_round(sizeof(MY_TYPE##_t));	\
        root->PRIV_SIZE = libcells_size_round(root->PRIV_SIZE);	\
        PRIVATE_INDEX = root->PRIV_SIZE;				\
        root->PRIV_SIZE += sizeof(struct MY_TYPE##_private);	\
        root->CODE_SIZE = sizeof(MY_TYPE##_c);			\
        root->_init = MY_TYPE##_init;					\
        init_code();							\
        __init_completed = true;					\
    }									\
    return C;								\
}									\
									\
const MY_TYPE##_c * MY_TYPE##_code(const_any_t cell) {		\
    return (const MY_TYPE##_c *) ((cell_t *)MY_TYPE##_check(cell))->C;\
}									\
									\
static inline struct MY_TYPE##_private * __(MY_TYPE##_t * self) {	\
    char * priv = (char *) ((cell_t *) self)->__;			\
    return (struct MY_TYPE##_private *) (priv + PRIVATE_INDEX);	\
}

/** Para uso no topo das estruturas de célula. */
#define SUPER_T(SUPER_TYPE)		\
  SUPER_TYPE##_t		super;	\
  const_any_t			BASE;

/** Para uso no topo das estruturas de código de célula. */
#define SUPER_C(SUPER_TYPE)		\
  SUPER_TYPE##_c		super;	\
  const_any_t			BASE;


#endif
