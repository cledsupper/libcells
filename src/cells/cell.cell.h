/** @file cell.cell.h
 * @brief Atalhos para métodos do dialeto Cell
 * @copyright Copyright 2022 Cledson Ferreira. All rights reserved.
 */

/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

/** A palavra <i>auto</i> torna a perda de vida da célula automática.
 *
 * Variáveis marcadas com "auto" vão ser passadas como argumento para
 * auto_cell_unref() quando a função retornar.
 *
 * IMPORTANTE: isto depende do suporte do compilador para funcionar!
 */
#define auto __attribute__((cleanup (auto_cell_unref)))

/** Atalho para cell_ref() com cast de tipo. */
#define ref(cell) ((typeof(cell)) cell_ref(cell))

/** Atalho para cell_unref() com cast de tipo. */
#define unref(cell) ((typeof(cell)) cell_unref(cell))

/** Atalho para cell_c::copy(). */
static inline any_t copy(any_t self, const_any_t cell) {
    return _(C, self, ->copy, cell);
}

/** Atalho para cell_c::cmp(). */
static inline int cmp(const_any_t self, const_any_t cell) {
    return _(C, self, ->cmp, cell);
}

/** Atalho para cell_c::tocstr(). */
static inline char * tocstr(const_any_t self) {
    return _(C, self, ->tocstr);
}
