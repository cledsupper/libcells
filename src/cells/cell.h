/** @file cell.h
 * @brief Estruturas da célula primordial.
 * @copyright Copyright 2022 Cledson Ferreira. All rights reserved.
 */

/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LESSO_CELL_H
#define _LESSO_CELL_H

#include <stdbool.h>
#include <stddef.h>
#include <libcells.h>

C_LANG_BLOCK()

/** Ponteiro para estrutura de célula.
 *
 * Estrutura de célula (any_t) é um tipo ambíguo que pode ser uma instância ou
 * um código da célula.
 */
typedef const void * const_any_t;
/** Ponteiro para estrutura de célula.
 *
 * Estrutura de célula (any_t) é um tipo ambíguo que pode ser uma instância ou
 * um código da célula.
 */
typedef void * any_t;


/** Estrutura de dados pública da instância da célula primitiva.
 *
 * Todas as instâncias derivadas contém uma cópia desta como primeiro atributo.
 *
 * Os primeiros dois atributos da célula correspondem à especificação any_t.
 */
typedef struct cell_public		cell_t;

/** Estrutura de dados pública do código da célula primitiva.
 *
 * Todos os códigos derivados contém uma cópia deste como primeiro atributo.
 *
 * Os primeiros dois atributos da célula correspondem à especificação any_t.
 */
typedef struct cell_code_public	cell_c;

struct cell_public {
    const_any_t	BASE;
    bool		IS_CELL;

    /** Código compartilhado entre instâncias da célula.
     *
     * O código não pode ser modificado após o procedimento interno
     * init_code().
     */
    const cell_c *	C;

    /** Dados privados.
     *
     * A primeira variável indica o tamanho da instância
     * (como size_t), caso alocada com libcells_malloc() ou equivalente.
     *
     * Para instâncias públicas e estáticas, os dados privados devem ter o
     * espaço de memória liberado e este ponteiro ser nulo. Isto é útil para
     * dados primitivos, como células que representam números ou strings
     * literais.
     */
    const_any_t	__;
};

/** Estrutura de dados privada da célula primitiva.
 *
 * Pode ser conveniente acessar um destes atributos diretamente em uma célula
 * derivada, portanto a declaração desta estrutura é pública.
 */
struct cell_private {
    size_t dynamic;
    volatile size_t life;
};

struct cell_code_public {
    const_any_t	BASE;
    bool		IS_CELL;


    /** Método virtual para cópia dos dados de instâncias de células.
     *
     * Uma dependência de cell_clone().
     *
     * Por padrão, a cópia é feita com memcpy().
     *
     * @param self célula de referência para cola.
     * @param cell célula para cópia.
     * @return self.
     */
    any_t (* copy)(any_t self, const_any_t cell);

    /** Método virtual puro que verifica diferenças entre as instâncias.
     *
     * A comparação deve ser feita conforme memcmp(), com células de tipos
     * equivalentes.
     *
     * @param self célula de referência é o primeiro parâmetro de comparação.
     * @param cell segundo parâmetro de comparação.
     * @return primeira diferença entre atributos de self e cell.
     */
    int (* cmp)(const_any_t self, const_any_t cell);


    /** Método virtual para finalização da instância.
     *
     * Remove referências, fecha arquivos etc., mas não libera a memória
     * ocupada pela instância.
     *
     * É recomendado que células que alteram este método o chamem ao final, com
     * a cópia do código da célula ancestral (super).
     *
     * @param self célula de referência.
     */
    void (* finalize)(any_t self);

    /** O método virtual puro tocstr() representa a célula com uma string.
     *
     * O ponteiro retornado deve ser descartado com libcells_free() ou equiv.
     *
     * @param self célula de referência.
     */
    char * (* tocstr)(const_any_t self);

    /** Método virtual para programação da célula.
     *
     * Constrói a instância e o código compartilhado.
     *
     * Métodos substituintes devem chamar o método do código ancestral antes
     * de inicializar os dados da célula.
     *
     * @param cell instância de célula nova.
     * @param code código celular a ser injetado na instância.
     */
    void (* _init)(any_t cell, any_t code);

    /** Uma string como: "libcells::cells::cell". */
    const char *	CELL_PATH;
    /** Uma string como: "cell". */
    const char *	CELL_TYPE;
    /** Um valor como sizeof(struct cell_public). */
    size_t		CELL_SIZE;
    /** Um valor como sizeof(struct cell_private). */
    size_t		PRIV_SIZE;
    /** Um valor como sizeof(struct cell_code_public). */
    size_t		CODE_SIZE;
};

/** Retorna uma cópia do código da célula. */
cell_c get_cell_code();


/** Se a estrutura corresponde a any_t. */
bool is_any(const_any_t any)
  NONNULL(1);

/** Se a estrutura corresponde a uma instância de célula. */
bool is_cell(const_any_t cell)
  NONNULL(1);

/** Se a estrutura corresponde a um código de célula. */
bool is_cell_code(const_any_t code)
  NONNULL(1);

/** Aborta caso o ponteiro não aponte para um any_t. */
const_any_t any_check(const_any_t any)
  NONNULL(1);

/** Aborta caso o ponteiro não aponte para uma instância de célula. */
cell_t * cell_check(const_any_t cell)
  NONNULL(1);

/** Aborta caso o ponteiro não aponte para um código de célula. */
const cell_c * cell_code_check(const_any_t code)
  NONNULL(1);


/** Retorna o tamanho da instância de célula.
 *
 * Caso any seja uma instância, retorna a soma do tamanhos reais ocupados por
 * atributos públicos e privados.
 *
 * Caso any seja um código, retorna a soma de tamanhos dos atributos públicos e
 * privados esperado para uma instância.
 *
 * @param any estrutura de célula.
 * @return tamanho da instância de célula.
 */
size_t sizeof_cell(const_any_t any)
  NONNULL(1);


/** Método final para programação do código e da instância da célula.
 *
 * Veja cell_c::_init() para detalhes.
 */
void cell_init(any_t cell, any_t code)
  NONNULL(1, 2);

/** Método final que nada faz.
 *
 * NÃO confundir com o método virtual cell_c::finalize()!
 */
void cell_finalize(any_t self)
  NONNULL(1);

/** Uma função conveniente para alocar memória e inicializar células. */
any_t cell_new(any_t code);

/** Método final que retorna uma cópia da célula.
 *
 * A cópia dos atributos é realizada com o método virtual cell_c::copy().
 */
any_t cell_clone(const_any_t self);

/** Incrementa o número de vidas da célula. */
cell_t * cell_ref(any_t self)
  NONNULL(1);

/** Decrementa o número de vidas da célula.
 *
 * Caso atinga o fim da vida, a instância é destruída, potencialmente liberando
 * a memória ocupada.
 *
 * Células qualificadas como públicas e estáticas são sempre finalizadas, pois
 * não é possível contabilizar vidas.
 */
cell_t * cell_unref(any_t self)
  NONNULL(1);

/** Método dependente de extensão do compilador para decrementar vidas.
 *
 * IMPORTANTE: isto depende do suporte do compilador para funcionar!
 * Pesquise a documentação do seu compilador para detalhes. Veja a palavra-
 * chave auto.
 *
 * @param cell uma instância ou um ponteiro para a instância.
 */
void auto_cell_unref(any_t cellptr)
  NONNULL(1);

/** Atalho para o código da célula com verificação de tipo. */
static inline const cell_c * cell_code(const_any_t cell) {
    return cell_check(cell)->C;
}

#define C cell_code

/** Atalho para o método virtual cell_c::copy(). */
static inline any_t cell_copy(any_t cell, const_any_t cell2) {
    return _(C, cell, ->copy, cell2);
}

/** Atalho para o método virtual cell_c::cmp(). */
static inline int cell_cmp(const_any_t cell, const_any_t cell2) {
    return _(C, cell, ->cmp, cell2);
}

/** Atalho para o método virtual cell_c::tocstr(). */
static inline char * cell_tocstr(const_any_t cell) {
    return _(C, cell, ->tocstr);
}

#ifdef USING_CELL_LANG
#include "cell.cell.h"
#endif // USING_CELL_LANG

#undef C

C_LANG_END()


#include "cell-codec-macros.h"

#endif
