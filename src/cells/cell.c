/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define USING_LIBCELLS_ABORT
#include "cell.h"

#undef __MODULE__
#define __MODULE__ "libcells::cells::cell"


static inline struct cell_private * __(cell_t * self) {
    return (struct cell_private *) self->__;
}


static any_t v_copy(any_t, const_any_t);
static int v_cmp(const_any_t, const_any_t);
static char * v_tocstr(const_any_t);

static cell_c C = {
  .BASE = NULL,
  .IS_CELL = false,

  .copy = v_copy,
  .cmp = v_cmp,
  .finalize = cell_finalize,
  .tocstr = v_tocstr,
  ._init = cell_init,

  .CELL_PATH = __MODULE__,
  .CELL_TYPE = "cell",
  .CELL_SIZE = sizeof(cell_t),
  .PRIV_SIZE = sizeof(struct cell_private),
  .CODE_SIZE = sizeof(cell_c)
};

cell_c get_cell_code() {
    if (C.BASE != &C) C.BASE = &C;
    return C;
}


static inline const_any_t _is_any(const_any_t any) {
    if (C.BASE != &C) return NULL;
    if (!any) return NULL;
    const cell_c * any_code = (const cell_c *) any;
    return any_code->BASE == C.BASE ?
      any
      : NULL;
}

static inline cell_t * _is_cell(const_any_t cell) {
    cell_t * self = (cell_t *) cell;

    return _is_any(self) ?
      (self->IS_CELL == true ? self : NULL)
      : NULL;
}

static inline const cell_c * _is_cell_code(const_any_t code) {
    const cell_c * c = (const cell_c *) code;
    return _is_any(code) ?
      (c->IS_CELL == false ? c : NULL)
      : NULL;
}


bool is_any(const_any_t any) {
    return _is_any(any) != NULL;
}

bool is_cell(const_any_t cell) {
    return _is_cell(cell) != NULL;
}

bool is_cell_code(const_any_t code) {
    return _is_cell_code(code) != NULL;
}

const_any_t any_check(const_any_t any) {
    if (!_is_any(any)) segfault("array/struct != 'any_t'");
    return any;
}

cell_t * cell_check(const_any_t cell) {
    if (!_is_cell(cell)) segfault("array/struct != 'cell'");
    return (cell_t *) cell;
}

const cell_c * cell_code_check(const_any_t code) {
    if (!_is_cell_code(code)) segfault("array/struct != 'cell'");
    return (const cell_c *) code;
}


size_t sizeof_cell(const_any_t any) {
    const cell_c * code;

    if (is_cell(any)) {
        cell_t * cell = (cell_t *) any;
        code = cell->C;
        struct cell_private * priv = __(cell);
        if (priv && priv->dynamic) return priv->dynamic;
    }
    else code = cell_code_check(any);

    return code->CELL_SIZE + code->PRIV_SIZE;
}

void cell_init(any_t cell, any_t code) {
    cell_t * self = (cell_t *) cell;
    cell_c * c = (cell_c *) code;

    assert(c->BASE == &C);

    *self = (cell_t) {
      .BASE = c->BASE,
      .IS_CELL = true,
      .C = c,
      .__ = libcells_malloc(c->PRIV_SIZE)
    };

    struct cell_private * priv = __(self);
    if (!priv) abort("failed to alloc memory for private data!");
    priv->life = 1;
    priv->dynamic = 0;
}

void cell_finalize(any_t cell) {}

any_t cell_new(any_t code) {
    cell_c * c = (cell_c *) cell_code_check(code);
    size_t size = c->CELL_SIZE + c->PRIV_SIZE;
    cell_t * self = (cell_t *) libcells_malloc(size);
    if (!self) abort("failed to alloc memory!");
    c->_init((any_t) self, code);

    struct cell_private * priv = (struct cell_private *) (((char *) self) + c->CELL_SIZE);
    memcpy(priv, self->__, c->PRIV_SIZE);
    libcells_free((void *) self->__);
    self->__ = (any_t) priv;

    priv->dynamic = size;

    return (any_t) self;
}

any_t cell_clone(const_any_t cell) {
    const cell_t * self = cell_check(cell);
    const cell_c * c = self->C;
    size_t size_dup = c->CELL_SIZE + c->PRIV_SIZE;
    cell_t * dup = (cell_t *) libcells_malloc(size_dup);
    if (!dup) abort("failed to alloc memory!");
    c->_init(dup, (any_t) c);

    libcells_free((void *)dup->__);
    dup->__ = ((char *) dup) + c->CELL_SIZE;
    __(dup)->dynamic = size_dup;

    return c->copy(dup, self);
}


cell_t * cell_ref(any_t cell) {
    struct cell_private * priv = __(cell_check(cell));
    if (priv) ++(priv->life);
    return (cell_t *) cell;
}

cell_t * cell_unref(any_t cell) {
    cell_t * self = cell_check(cell);
    struct cell_private * priv = __(self);
    const cell_c * c = self->C;
    size_t res = 0;

    if (priv) res = --(priv->life);
    if (!res) {
        c->finalize(cell);
        if (priv) {
            self->BASE = NULL;
            if (priv->dynamic) libcells_free(cell);
            else {
                libcells_free(priv);
                self->__ = NULL;
            }
        }
        self = NULL;
    }

    return self;
}

void auto_cell_unref(any_t cell) {
    if (*((any_t *) cell)) {
        if (!is_cell(cell))
            cell = *((any_t *) cell);
        cell_unref(cell);
    }
}


static any_t v_copy(any_t cell, const_any_t cell2) {
    if (cell == cell2) return cell;

    cell_t * c1 = (cell_t *) cell;
    cell_t * c2 = cell_check(cell2);

    if (c2->C->CELL_SIZE > c1->C->CELL_SIZE)
        segfault(
          "arg 2 has bigger public data: .copy(1: <%s>[%zu], 2: <%s>[%zu])",
          c1->C->CELL_TYPE,
          c1->C->CELL_SIZE,
          c2->C->CELL_TYPE,
          c2->C->CELL_SIZE
        );
    if (c2->C->PRIV_SIZE > c1->C->PRIV_SIZE)
        segfault(
          "arg 2 has bigger private data: .copy(1: <%s>[%zu], 2: <%s>[%zu])",
          c1->C->CELL_TYPE,
          c1->C->PRIV_SIZE,
          c2->C->CELL_TYPE,
          c2->C->PRIV_SIZE
        );

    memcpy(
      ((char *) c1) + sizeof(cell_t),
      ((char *) c2) + sizeof(cell_t),
      c2->C->CELL_SIZE - sizeof(cell_t)
    );
    if (c2->__)
        memcpy(
          ((char *) c1->__) + sizeof(struct cell_private),
          ((char *) c2->__) + sizeof(struct cell_private),
          c2->C->PRIV_SIZE - sizeof(struct cell_private)
        );
    return cell;
}

static int v_cmp(const_any_t cell, const_any_t cell2) {
    segfault("not implemented for <%s>", ((cell_t *)cell)->C->CELL_TYPE);
    return -1;
}

static char * v_tocstr(const_any_t cell) {
    char * data = NULL;
    const cell_c * c = ((cell_t *) cell)->C;
    int dlen = snprintf(
      data,
      0,
      "%s::%s {%s}",
      c->CELL_PATH,
      c->CELL_TYPE,
      c == &C ? "" : "?"
    );
    data = le_array(char, dlen+1);
    if (!data) abort("OUT OF MEMORY");
    sprintf(
      data,
      "%s::%s {%s}",
      c->CELL_PATH,
      c->CELL_TYPE,
      c == &C ? "" : "?"
    );

    return data;
}
