/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libcells.h"

//static const char WARNI_FORMAT[] = " WARN.: %s[%d]: %s(): %s\n";
//static const char ERROR_FORMAT[] = " ERROR: %s[%d]: %s(): %s\n";
static const char FATAL_FORMAT[] = " FATAL: %s[%d]: %s(): %s\n";

void libcells_verrf(
  const char logf[],
  const char path[],
  const char foo[],
  int line,
  const char msgf[],
  va_list vl
) {
    char * sfmt = NULL;
    int slen = snprintf(sfmt, 0, logf, path, line, foo, msgf);
    if (slen <= 0) {
        fputs(" FATAL: libcells_verrf(): snprintf() error", stderr);
        abort();
    }

    sfmt = le_array(char, slen+1);
    if (!sfmt) {
        fputs(
          " FATAL: libcells_verrf(): OUT OF MEMORY",
          stderr
        );
        abort();
    }
    snprintf(sfmt, slen+1, logf, path, line, foo, msgf);

    vfprintf(stderr, sfmt, vl);
    le_free(sfmt);
}

void libcells_abort(
  const char path[],
  const char foo[],
  int line,
  const char msgf[],
  ...
) {
    va_list vl;

    va_start(vl, msgf);
    libcells_verrf(FATAL_FORMAT, path, foo, line, msgf, vl);
    va_end(vl);

    abort();
}

void libcells_segfault(
  const char path[],
  const char foo[],
  int line,
  const char msgf[],
  ...
) {
    va_list vl;

    va_start(vl, msgf);
    libcells_verrf(FATAL_FORMAT, path, foo, line, msgf, vl);
    va_end(vl);

    raise(SIGSEGV);
}


#define assert_non_null(VAR) if (!VAR) libcells_abort(\
  __MODULE__,\
  __func__,\
  __LINE__,\
  "assertion (%s != NULL) fail!",\
  #VAR\
)

static struct libcells_minterface mem = {
    .malloc = malloc,
    .realloc = realloc,
    .free = free,
    .count = 0
};


void libcells_minterface_set(struct libcells_minterface iface) {
    assert_non_null(iface.malloc);
    assert_non_null(iface.realloc);
    assert_non_null(iface.free);

    mem.malloc = iface.malloc;
    mem.realloc = iface.realloc;
    mem.free = iface.free;

    mem.count = 0;
}

struct libcells_minterface libcells_minterface_get() {
    return mem;
}

size_t libcells_mcount() {
    #ifdef NDEBUG
      fprintf(stderr, " WARN: %s(): always return 0 when NDEBUG is set\n", __func__);
    #endif
    return mem.count;
}

void * libcells_calloc(size_t nelems, size_t size) {
    size_t bytes = nelems*size;
    if (!bytes) return NULL;
    void * p = mem.malloc(bytes);
    if (!p) return p;
    DEBUG(++mem.count);
    memset(p, 0, bytes);
    return p;
}

void * libcells_malloc(size_t bytes) {
    void * p = mem.malloc(bytes);
    DEBUG(if (p) ++mem.count);
    return p;
}

void * libcells_realloc(void * old, size_t bytes) {
    void * p = mem.realloc(old, bytes);
    DEBUG(
      if (!old && p) ++mem.count;
      else if (old && !bytes) --mem.count;
    )
    return p;
}

void libcells_free(void * old) {
    mem.free(old);
    DEBUG(--mem.count);
}

static const size_t P = sizeof(void*);

size_t libcells_size_round(size_t s) {
    return (s%P) != 0 ?
      s+(P - (s%P))
      : s;
}
