/** @file libcells.h
 * @brief Código de suporte para as células.
 * @copyright Copyright 2022 Cledson Ferreira. All rights reserved.
 */

/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LESSO_LIBCELLS_H
#define _LESSO_LIBCELLS_H

#define NONNULL(...) __attribute__((nonnull (__VA_ARGS__)))
#define __MODULE__ __FILE__

#ifdef __cplusplus
  #define C_LANG_BLOCK() extern "C" {
  #define C_LANG_END() }
#else
  #define C_LANG_BLOCK()
  #define C_LANG_END()
#endif

#ifdef NDEBUG
  #define DEBUG(...)
#else
  #define DEBUG(...) __VA_ARGS__
#endif

#define le_new(TYPE) ((TYPE *) libcells_malloc(sizeof(TYPE)))
#define le_array(TYPE, N) ((TYPE *) libcells_calloc(N, sizeof(TYPE)))
#define le_resize(POINTER, N) \
  ( (typeof(POINTER)) libcells_realloc((POINTER), (N)*sizeof(*(POINTER))) )
#define le_free(POINTER) libcells_free(POINTER)

#define _(IF, CELL, DO, ...) IF (CELL) DO (CELL ,##__VA_ARGS__ )

#if defined(USING_CELL_LANG) || defined(USING_LIBCELLS_ABORT)
  #define assert(EXPR) if (!(EXPR)) libcells_abort(\
    __MODULE__,					\
    __func__,						\
    __LINE__,						\
    "assertion (%s) fail!",				\
    #EXPR						\
  )

  #define assert_non_null(VAR) if (!VAR) libcells_abort(\
    __MODULE__,					\
    __func__,						\
    __LINE__,						\
    "assertion (%s != NULL) fail!",			\
    #VAR						\
  )

  #define abort(format, ...) libcells_abort(		\
    __MODULE__,					\
    __func__,						\
    __LINE__,						\
    format ,##__VA_ARGS__				\
  )

  #define segfault(format, ...) libcells_segfault(	\
    __MODULE__,					\
    __func__,						\
    __LINE__,						\
    format ,##__VA_ARGS__				\
  )

#endif // USING_CELL_LANG || USING_LIBCELLS_ABORT

C_LANG_BLOCK()


void libcells_abort(
  const char path[],
  const char foo[],
  int line,
  const char format[],
  ...
) __attribute__((noreturn, format (printf, 4, 5)));

void libcells_segfault(
  const char path[],
  const char foo[],
  int line,
  const char format[],
  ...
) __attribute__((format (printf, 4, 5)));


struct libcells_minterface {
    void * (* malloc)(size_t);
    void * (* realloc)(void *, size_t);
    void (* free)(void *);
    volatile size_t count;
};

void libcells_minterface_set(struct libcells_minterface iface);

struct libcells_minterface libcells_minterface_get();

size_t libcells_mcount();

void * libcells_calloc(size_t nelems, size_t size);

void * libcells_malloc(size_t bytes);

void * libcells_realloc(void * old, size_t bytes);

void libcells_free(void * old)
  NONNULL(1);

size_t libcells_size_round(size_t bytes);


C_LANG_END()

#endif
