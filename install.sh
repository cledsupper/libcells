#!/bin/sh
if [ -z "$PREFIX" -a "$(id -u)" -ne 0 ]; then
  echo "Execute como root:"
  echo "\$ sudo $0 $@"
  exit 1
fi

[ -z "$PREFIX" ] && PREFIX="/usr/local"

do_install() {
  echo "Confirmar existência da biblioteca compartilhada"
  [ ! -r ./build/libcells.so ] && exit 1

  echo "Garantir diretório $PREFIX/lib"
  mkdir -p "$PREFIX/lib"

  echo "Copiar biblioteca"
  cp ./build/libcells.so "$PREFIX/lib/"

  echo "Garantir diretório $PREFIX/include/cells"
  mkdir -p "$PREFIX/include/cells"

  echo "Copiar arquivos"
  cp ./src/libcells.h "$PREFIX/include/"
  cp ./src/cells/*.h "$PREFIX/include/cells/"

  echo "Execute o comando abaixo caso programas não executem:"
  echo "# ldconfig"
}

do_remove() {
  echo "Remover arquivos e diretórios"
  rm "$PREFIX/lib/libcells.so"
  rm "$PREFIX"/include/cells/*.h
  rmdir "$PREFIX/include/cells"
  rm "$PREFIX/include/libcells.h"
}

case "$1" in
  remove) do_remove ;;
  install) do_install ;;
  *)
    echo "USO: $0 < install | remove >"
    exit 2
    ;;
esac

echo
echo "Finalizado!"
echo "by cleds.upper"
