#!/bin/sh

if [ ! -f "./src/libcells.h" ]; then
    echo "ERR: execute isto na raiz do projeto!" >&2
    exit 1
fi

CC=clang
CCLIBS="-L $(pwd)/build -lcells"
CCFLAGS="-Wall -I../src"

echo_run () {
    echo "> $@"
    "$@"
    local e=$?
    [ $e -ne 0 -a "$1" != "rm" ] && exit $e
    return $e
}

library () {
  echo_run rm *.o *.so *.run
  echo_run $CC -fPIC $CCFLAGS -c ../src/libcells.c
  echo_run $CC -fPIC $CCFLAGS -c ../src/cells/cell.c
  echo_run $CC -shared -o libcells.so *.o
  return $?
}

run () {
  echo_run $CC $CCLIBS -o main.run $CCFLAGS ../src/main.c
  echo_run export LD_LIBRARY_PATH="$(pwd)"
  echo_run ./main.run
  return $?
}


echo_run mkdir -p build
echo_run cd build/

case "$1" in
  lib|library) library ;;
  run) run ;;
  *)
    echo "USO: $0 [ lib(rary) | run ]"
    exit 1
    ;;
esac

exit $?
