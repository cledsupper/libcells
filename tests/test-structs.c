/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This file is part of the Cells library.
 *
 * The Cells library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include <cells/cell.h>

DECLARE(my_cell)

struct my_cell_public {
    SUPER_T(cell)
    void * p;
    int a;
    char b;
};

struct my_cell_private {
    int a;
    char b;
};

struct my_cell_code_public {
    SUPER_C(cell)
};

DEFINE(cell, my_cell)

static void init_code() {}

static void init(my_cell_t * self) {}

int main() {
    get_my_cell_code();
    cell_c * c = &C.super;

    printf("struct my_cell_public:  %zu\n", sizeof(my_cell_t));
    printf("by my_cell.CELL_SIZE:   %zu\n", c->CELL_SIZE);
    puts("");
    printf("struct my_cell_private: %zu\n", sizeof(struct my_cell_private));
    printf("by my_cell.PRIV_SIZE:   %zu\n", c->PRIV_SIZE);
    printf("begin of private:       %zu\n", PRIVATE_INDEX);

    return 0;
}
