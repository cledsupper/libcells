/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This file is part of the Cells library.
 *
 * The Cells library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#define USING_CELL_LANG
#define USING_LIBCELLS_ABORT
#include "contact.h"

struct contact_private {
    char name[128];
    char email[64];
    char phone[16];
};

DEFINE(cell, contact)

contact_t contact() {
    contact_t self;
    contact_init(&self, &C);
    return self;
}

static char * v_tocstr(const_any_t cell) {
    struct contact_private * priv = __((contact_t *) cell);
    char * data = le_array(char, 256);
    if (!data) abort("OUT OF MEMORY!");

    size_t len = snprintf(
      data,
      256,
      "\"%s\": {\n\t email: \"%s\";\n\t phone: \"%s\";\n}",
      priv->name,
      priv->email,
      priv->phone
    );

    return le_resize(data, len+1);
}

static unsigned int user_id;

static void init_code() {
    user_id = 0;
    C.super.tocstr = v_tocstr;
}

static void init(contact_t * self) {
    struct contact_private * priv = __(self);
    sprintf(priv->name, "New User %u", ++user_id);
    sprintf(priv->email, "user%d@examp.le", user_id);
    sprintf(priv->phone, "+55%011u", user_id + 1000000000);
}
