/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This file is part of the Cells library.
 *
 * The Cells library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "copy/contact.h"

int main() {
    auto contact_t u1 = contact();
    char * r = tocstr(&u1);
    puts(r);
    le_free(r);
    printf("%zu\n", libcells_mcount());
    return 0;
}
