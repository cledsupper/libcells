/*
 * Copyright (C) 2022  Cledson Ferreira
 *
 * This file is part of the Cells library.
 *
 * The Cells library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#define USING_CELL_LANG
#define USING_LIBCELLS_ABORT
#include <cells/cell.h>

DECLARE(literal)

struct literal_public {
    SUPER_T(cell)

    const char * cstr;
    size_t len;
};

struct literal_private {};

struct literal_code_public {
    SUPER_C(cell)
};

DEFINE(cell, literal)

static int v_cmp(const_any_t, const_any_t);

static void init_code() {
    C.super.cmp = v_cmp;
}

static void init(literal_t * self) {
    self->cstr = "";
    self->len = 0;
}

literal_t literal(const char cstr[]) {
    literal_t liter;
    literal_init(&liter, &C);

    /* células públicas e locais não precisam de dados privados */
    libcells_free((void *) liter.super.__);
    liter.super.__ = NULL;

    liter.cstr = cstr;
    liter.len = strlen(cstr);

    return liter;
}

int main() {
    auto literal_t s0 = literal("lesso");
    auto literal_t s1 = literal("lesdo");

    printf(
      "OPERAÇÕES BÁSICAS DE CÉLULA COM <%s>\n",
      s0.super.C->CELL_TYPE
    );

    char * cs = tocstr(&s0);
    puts(cs);
    le_free(cs);

    copy(&s1, &s0);

    printf("s0 == s1: %d\n", cmp(&s0, &s1));

    printf("\nis_literal(\"s\"): %d\n", is_literal("s"));

    return 1;
}

static int v_cmp(const_any_t a, const_any_t b) {
    if (a == b) return 0;

    const literal_t * strA = (literal_t *) a;
    const literal_t * strB = literal_check(b);

    if (strA->cstr == strB->cstr) return 0;

    size_t len = strA->len < strB->len ? strA->len : strB->len;

    return strncmp(strA->cstr, strB->cstr, len);
}
